#include "circulo.hpp"
#include <iostream>
using namespace std;

        Circulo :: Circulo(){
        set_tipo("Círculo Genérico");
        set_altura(0);
        set_base(0);
        set_raio(1);
        }
        Circulo :: Circulo(float raio){
        set_tipo("Círculo");
        set_altura(0);
        set_base(0);
        set_raio(raio);
        }
        float Circulo::  get_raio(){
            return raio;
        }
        void Circulo ::  set_raio(float raio){
            this -> raio = raio; 
        } 
        float Circulo :: calcula_area() {
            return (3.14 * get_raio()* get_raio()); 
        }
        float Circulo :: calcula_perimetro(){
            return 6.28 * raio; 
        }
       void Circulo :: imprime() {
          cout  << "Tipo: " << get_tipo() << endl; 
          cout<< "Área: " << calcula_area() << endl;
          cout << "Perímetro:  " << calcula_perimetro() << endl; 
            
       }