#include "triangulo.hpp"
#include <iostream>


Triangulo::Triangulo(){
    set_tipo("Triangulo Genérico");
    set_base(1);
    set_altura(1);
}
Triangulo :: Triangulo(float base, float altura){
    set_tipo("Triangulo");
    set_base(base);
    set_altura(altura);
}
    
    float Triangulo :: calcula_area(){
        return (get_base()*get_altura())/2; 
    }
    float Triangulo:: calcula_perimetro(){
        return 3* get_base(); 
    }
    void Triangulo :: imprime(){
     cout << "Tipo: " << get_tipo() << endl; 
     cout << "Área: " <<calcula_area () << endl;
     cout << "Perímetro: " << calcula_perimetro () << endl; 
 }