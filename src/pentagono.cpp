#include <iostream>
#include "pentagono.hpp"
#include <math.h> 

 Pentagono :: Pentagono(){
    tipo = "Pentágono Genérico";
    base = 10.0f;
    altura = 5.0;
    lado = 1;
 }
 Pentagono ::  Pentagono(float lado){
          set_tipo("Pentágono");
          set_base(1); 
          this -> lado = lado; 
  }
  void Pentagono :: set_lado(float lado){
      this -> lado = lado; 
  }
  float Pentagono :: get_lado() {
      return lado; 
  }
    float  Pentagono :: calcula_area(){
        return get_lado()*get_lado()*sqrt(25+10*sqrt(5))/4; 
    }
        float Pentagono :: calcula_perimetro(){
            return get_lado()*5;
        }

void Pentagono :: imprime(){
     cout << "Tipo: " << get_tipo() << endl; 
     cout << "Área: " <<calcula_area () << endl;
     cout << "Perímetro: " << calcula_perimetro () << endl; 
 }