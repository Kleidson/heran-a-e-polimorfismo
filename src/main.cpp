#include <iostream>
#include <vector>
#include "formageometrica.hpp"
#include "quadrado.hpp"
#include "circulo.hpp"
#include "triangulo.hpp"
#include "paralelogramo.hpp"
#include "pentagono.hpp"
#include "hexagono.hpp"

using namespace std; 
int main (int argc, char ** argv ){
    
    vector <FormaGeometrica *> lista; 
    lista.push_back(new Triangulo()); 
    lista.push_back(new Triangulo(2.5,3)); 
    lista.push_back(new Quadrado());
    lista.push_back(new Quadrado(5.7)); 
    lista.push_back(new Circulo());
    lista.push_back(new Circulo(4.9)); 
    lista.push_back(new Paralelogramo());
    lista.push_back(new Paralelogramo(1.1,2.8,5.7));
    lista.push_back(new Pentagono()); 
    lista.push_back(new Pentagono(5.9)); 
    lista.push_back(new Hexagono());
    lista.push_back(new Hexagono(1.9)); 
    

    int a = lista.size();
    for(int i =0; i<a ; i++){
        lista[i] -> imprime(); 
       cout << "---------------------------------------------------------" << endl; 
    }



    
    return 0; 
}