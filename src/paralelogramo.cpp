#include <iostream>
#include "paralelogramo.hpp"

    Paralelogramo :: Paralelogramo(){
        set_tipo("Paralelogramo Genérico"); 
        set_altura(1);
        set_base(1);
        set_lado(1);
    }
    Paralelogramo :: Paralelogramo (float base,float lado, float altura){
        set_tipo("Paralelogramo"); 
        set_altura(altura);
        set_base(base);
        set_lado(lado); 
    }
    void Paralelogramo:: set_lado(float lado){
        this -> lado = lado;
    }
    
    float Paralelogramo::  get_lado(){
        return lado; 
    } 

    float Paralelogramo::  calcula_perimetro(){
        return (get_lado()+get_base())*2;
    }
    void Paralelogramo :: imprime(){
          cout  << "Tipo: " << get_tipo() << endl; 
          cout<< "Área: " << calcula_area() << endl;
          cout << "Perímetro:  " << calcula_perimetro() << endl;      
       }
    