#include <iostream>
#include <math.h> 
#include "hexagono.hpp"


using namespace std; 

     Hexagono::Hexagono() {
         set_tipo("Hexágono Genérico");
         set_base(1);
         set_altura(1);
         set_lado(1); 
     }
    Hexagono :: Hexagono(float lado){
        set_tipo("Hexágono");
         set_base(1);
         set_altura(1);
        this -> lado = lado; 
    }
    void Hexagono :: set_lado(float lado){
        this -> lado = lado; 
    }
    float Hexagono :: get_lado(){
        return lado; 
    }
    float Hexagono ::  calcula_area(){
        return get_lado()*get_lado() * 3*sqrt(3)/2;
    }
    float Hexagono :: calcula_perimetro(){
        return 6*get_lado(); 
    }
    void  Hexagono :: imprime(){
     cout << "Tipo: " << get_tipo() << endl; 
     cout << "Área: " <<calcula_area () << endl;
     cout << "Perímetro: " << calcula_perimetro () << endl; 
    }
