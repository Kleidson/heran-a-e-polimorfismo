#include "quadrado.hpp"
#include <iostream>

    Quadrado:: Quadrado() {
        set_tipo ("Quadrado Genérico");
        set_altura(1);
        set_base(1);
    }
    Quadrado :: Quadrado(float base) {
        set_tipo ("Quadrado");
        set_altura(base);
        set_base(base);
    }
    void Quadrado :: imprime() {
          cout  << "Tipo: " << get_tipo() << endl; 
          cout<< "Área: " << calcula_area() << endl;
          cout << "Perímetro:  " << calcula_perimetro() << endl; 
         
            
       }