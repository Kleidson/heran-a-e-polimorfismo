#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"


class Hexagono : public FormaGeometrica{
    protected: 
        float lado; 
    public: 
    Hexagono();
    Hexagono(float altura); 
    void set_lado(float lado); 
    float get_lado(); 
    float calcula_area();
    float calcula_perimetro(); 
    void imprime();
}; 

#endif 