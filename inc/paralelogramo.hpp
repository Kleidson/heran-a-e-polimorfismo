#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP
#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica {
    protected:
    float lado; 
    public: 
    Paralelogramo();
    Paralelogramo (float base,float lado,  float altura);
    void set_lado(float lado);
    float get_lado(); 
   
    float calcula_perimetro();
    void imprime(); 
}; 

#endif