#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica{
    protected :
        float raio; 
    public :
        Circulo();
        Circulo(float raio); 
        float get_raio();
        void set_raio(float raio); 
        float calcula_area();
        float calcula_perimetro();
        void imprime();  

};


#endif 